package com.ram0250.practice.oleg.sqlitedbapp;

import com.ram0250.practice.oleg.sqlitedbapp.logic.Controller;
import com.ram0250.practice.oleg.sqlitedbapp.pojo.DataSourceInterface;
import com.ram0250.practice.oleg.sqlitedbapp.pojo.ListItemObject;
import com.ram0250.practice.oleg.sqlitedbapp.view.ViewInterface;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(MockitoJUnitRunner.class)
public class ControllerUnitTest {

    /**
     * Test Double:
     *  Specifically a "Mock"
     */
    @Mock
    DataSourceInterface dataSource;

    @Mock
    ViewInterface view;

    Controller controller;

    private static final ListItemObject testListItemObject = new ListItemObject(
            "Titanic",
            "Drama, Romance",
            1997,
            R.color.GREEN
    );

    @Before
    public void setUpTest() {
        MockitoAnnotations.initMocks(this);
        controller = new Controller(view, dataSource);
    }

    @Test
    public void onGetListDataSuccessful() throws Exception {
        // Set up data for test
        ArrayList<ListItemObject> listOfData = new ArrayList<>();
        listOfData.add(testListItemObject);

        // Set up Mocks to return data we want
        Mockito.when(dataSource.getListOfData()).thenReturn(listOfData);

        //Call the method (unit) we are testing
        controller.getListFromDataSource();

        // Check how the Tested class responds to the data it receives or test it's behaviour
        Mockito.verify(view).setUpAdapterAndView(listOfData);
    }

    @Test
    public void onListItemClicked() {
        controller.onListItemClick(testListItemObject);

        Mockito.verify(view).startDetailActivity(
                testListItemObject.getTitle(),
                testListItemObject.getGenres(),
                testListItemObject.getYear(),
                testListItemObject.getColorR()
        );
    }
}