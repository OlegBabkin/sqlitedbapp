package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;

import java.util.List;

@Dao
public interface FilmDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Film... films);

    @Update
    void updateMany(Film... films);

    @Delete
    void deleteMany(Film... films);

    @Query("SELECT * FROM films")
    List<Film> getAll();
}
