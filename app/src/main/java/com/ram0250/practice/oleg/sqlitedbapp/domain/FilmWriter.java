package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "film_writer",
        primaryKeys = {"film_id", "writer_id"},
        foreignKeys = {
                @ForeignKey(
                        entity = Film.class,
                        parentColumns = "id",
                        childColumns = "film_id",
                        onDelete = CASCADE
                ),
                @ForeignKey(
                        entity = Writer.class,
                        parentColumns = "id",
                        childColumns = "writer_id",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index(value = "film_id"),
                @Index(value = "writer_id")
        }
)
public class FilmWriter {

    @ColumnInfo(name = "film_id")
    private long filmId;

    @ColumnInfo(name = "writer_id")
    private long writerId;

    public FilmWriter() { }

    @Ignore
    public FilmWriter(long filmId, long writerId) {
        this.filmId = filmId;
        this.writerId = writerId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getWriterId() {
        return writerId;
    }

    public void setWriterId(long writerId) {
        this.writerId = writerId;
    }
}
