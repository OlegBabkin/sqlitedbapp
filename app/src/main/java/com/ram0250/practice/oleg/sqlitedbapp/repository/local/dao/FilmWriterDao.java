package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmWriter;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Writer;

import java.util.List;

@Dao
public interface FilmWriterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(FilmWriter... filmWriters);

    @Update
    void updateMany(FilmWriter... filmWriters);

    @Delete
    void deleteMany(FilmWriter... filmWriters);

    @Query(
            "SELECT writers.* FROM writers\n"
                    + "INNER JOIN film_writer ON writers.id = film_writer.writer_id\n"
                    + "WHERE film_writer.film_id = :id"
    )
    List<Writer> getWritersByFilmId(long id);

    @Query(
            "SELECT films.* FROM films\n"
                    + "INNER JOIN film_writer ON films.id = film_writer.film_id\n"
                    + "WHERE film_writer.writer_id = :id"
    )
    List<Film> getFilmsByWriterId(long id);
}
