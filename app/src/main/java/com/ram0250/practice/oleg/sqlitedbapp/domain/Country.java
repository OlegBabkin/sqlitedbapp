package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

@Entity(tableName = "countries")
public class Country extends Identifiable {

    @ColumnInfo(name = "country_name")
    private String countryName;

    public Country() { }

    @Ignore
    public Country(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
