package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "film_actor",
        primaryKeys = {"film_id", "actor_id"},
        foreignKeys = {
                @ForeignKey(
                        entity = Film.class,
                        parentColumns = "id",
                        childColumns = "film_id",
                        onDelete = CASCADE
                ),
                @ForeignKey(
                        entity = Actor.class,
                        parentColumns = "id",
                        childColumns = "actor_id",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index(value = "film_id"),
                @Index(value = "actor_id")
        }
)
public class FilmActor {

    @ColumnInfo(name = "film_id")
    private long filmId;

    @ColumnInfo(name = "actor_id")
    private long actorId;

    public FilmActor() { }

    @Ignore
    public FilmActor(long filmId, long actorId) {
        this.filmId = filmId;
        this.actorId = actorId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getActorId() {
        return actorId;
    }

    public void setActorId(long actorId) {
        this.actorId = actorId;
    }
}
