package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Genre;

import java.util.List;

@Dao
public interface GenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Genre... genres);

    @Update
    void updateMany(Genre... genres);

    @Delete
    void deleteMany(Genre... genres);

    @Query("SELECT * FROM genres")
    List<Genre> getAll();
}
