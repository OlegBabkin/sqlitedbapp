package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

@Entity(tableName = "actors")
public class Actor extends Person{

    public Actor() { }

    @Ignore
    public Actor(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
