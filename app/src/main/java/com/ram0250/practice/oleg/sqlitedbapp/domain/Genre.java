package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

@Entity(tableName = "genres")
public class Genre extends Identifiable {

    @ColumnInfo(name = "genre_name")
    private String genreName;

    public Genre() { }

    @Ignore
    public Genre(String genreName) {
        this.genreName = genreName;
    }

    public String getGenreName() { return genreName; }

    public void setGenreName(String genreName) { this.genreName = genreName; }
}
