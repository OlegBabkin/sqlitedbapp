package com.ram0250.practice.oleg.sqlitedbapp.logic;

import android.view.View;

import com.ram0250.practice.oleg.sqlitedbapp.pojo.DataSourceInterface;
import com.ram0250.practice.oleg.sqlitedbapp.pojo.ListItemObject;
import com.ram0250.practice.oleg.sqlitedbapp.view.ViewInterface;

public class Controller {

    private ListItemObject temporaryListItemObject;
    private int temporaryListItemPosition;

    private ViewInterface view;
    private DataSourceInterface dataSource;

    /**
     * As soon as this object is created, it does a few things:
     * 1. Assigns Interfaces Variables so that it can talk to the DataSource and that Activity
     * 2. Tells the dataSource to fetch a List of ListItems.
     * 3. Tells the View to draw the fetched List of Data.
     * @param view
     * @param dataSource
     */
    public Controller(ViewInterface view, DataSourceInterface dataSource) {
        this.view = view;
        this.dataSource = dataSource;
        getListFromDataSource();
    }

    /**
     * In a real App, I would normally talk to this DataSource using RxJava 2. This is because most
     * calls to Services like a Database/Server should be executed on a seperate thread that the
     * mainThread (UI Thread). See my full projects for examples of this.
     */
    public void getListFromDataSource() {
        view.setUpAdapterAndView(dataSource.getListOfData());
    }

    public void onListItemClick(ListItemObject testListItemObject, View viewRoot) {
        view.startDetailActivity(
                testListItemObject.getTitle(),
                testListItemObject.getGenres(),
                testListItemObject.getYear(),
                testListItemObject.getColorR(),
                viewRoot
        );
    }

    public void createNewListItem() {
        /*
        To simulate telling the DataSource to create a new record and waiting for it's response,
        we'll simply have it return a new ListItem.
        In a real App, I'd use RxJava 2 (or some other
        API/Framework for Asynchronous Communication) to have the Datasource do this on the
        IO thread, and respond via an Asynchronous callback to the Main thread.
        */
        ListItemObject newListItem = dataSource.createNewListItem();
        view.addNewListItemToView(newListItem);
    }

    public void onListItemSwiped(int position, ListItemObject listItemObject) {
        dataSource.deleteListItem(listItemObject);
        view.deleteListItemAt(position);

        temporaryListItemObject = listItemObject;
        temporaryListItemPosition = position;

        view.showUndoSnackbar();
    }

    public void onUndoConfirmed() {
        if (temporaryListItemObject != null) {
            dataSource.insertListItem(temporaryListItemObject);
            view.insertListItemAt(temporaryListItemPosition, temporaryListItemObject);
            temporaryListItemObject = null;
            temporaryListItemPosition = 0;
        } else {

        }
    }

    public void onSnackbarTimeout() {
        temporaryListItemObject = null;
        temporaryListItemPosition = 0;
    }
}
