package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Country;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmCountry;

import java.util.List;

@Dao
public interface FilmCountryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(FilmCountry... filmCountries);

    @Update
    void updateMany(FilmCountry... filmCountries);

    @Delete
    void deleteMany(FilmCountry... filmCountries);

    @Query("SELECT * FROM film_country")
    List<FilmCountry> getAll();

    @Query(
            "SELECT countries.* FROM countries\n"
                    + "INNER JOIN film_country ON countries.id = film_country.country_id\n"
                    + "WHERE film_country.film_id = :id"
    )
    List<Country> getCountriesByFilmId(long id);

    @Query(
            "SELECT films.* FROM films\n"
                    + "INNER JOIN film_country ON films.id = film_country.film_id\n"
                    + "WHERE film_country.country_id = :id"
    )
    List<Film> getFilmsByCountryId(long id);
}
