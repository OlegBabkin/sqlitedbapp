package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Actor;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmActor;

import java.util.List;

@Dao
public interface FilmActorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(FilmActor... filmActors);

    @Update
    void updateMany(FilmActor... filmActors);

    @Delete
    void deleteMany(FilmActor... filmActors);

    @Query(
            "SELECT actors.* FROM actors\n"
                    + "INNER JOIN film_actor ON actors.id = film_actor.actor_id\n"
                    + "WHERE film_actor.film_id = :id"
    )
    List<Actor> getActorsByFilmId(long id);

    @Query(
            "SELECT films.* FROM films\n"
                    + "INNER JOIN film_actor ON films.id = film_actor.film_id\n"
                    + "WHERE film_actor.actor_id = :id"
    )
    List<Film> getFilmsByActorId(long id);
}
