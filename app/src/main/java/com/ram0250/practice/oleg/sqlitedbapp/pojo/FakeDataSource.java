package com.ram0250.practice.oleg.sqlitedbapp.pojo;

import com.ram0250.practice.oleg.sqlitedbapp.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Test Double
 * (Specifically a "Fake")
 * Created by oleg.babkin on 17 Nov 2017.
 */

public class FakeDataSource implements DataSourceInterface{

    private static final int SIZE_OF_COLLECTION = 10;
    private Random random;

    private final String[] titles = {
            "Titanic",
            "Avatar",
            "Aliens",
            "The Pianist",
            "The Green Mile",
            "Pearl Harbor",
            "The Grand Budapest Hotel",
            "Gravity",
            "Alice in Wonderland",
            "Shakespeare in Love"
    };

    private final String[] genres = {
            "Drama, Romance",
            "Action, Adventure, Fantasy",
            "Action, Adventure, Sci-Fi",
            "Biography, Drama, War",
            "Crime, Drama, Fantasy",
            "Action, Drama, History",
            "Adventure, Comedy, Crime",
            "Adventure, Drama, Sci-Fi",
            "Adventure, Family, Fantasy",
            "Comedy, Drama, Romance"
    };

    private final int[] years = {
            1997,
            2009,
            1986,
            2002,
            1999,
            2001,
            2014,
            2013,
            2010,
            1998,
    };

    private final int[] drawables = {
            R.drawable.red_drawable,
            R.drawable.green_drawable,
            R.drawable.blue_drawable,
            R.drawable.yellow_drawable
    };


    public FakeDataSource() {
        random = new Random();
    }

    @Override
    public List<ListItemObject> getListOfData() {
        ArrayList<ListItemObject> listOfData = new ArrayList<>();

        for (int i = 0; i < SIZE_OF_COLLECTION; i++) {
            ListItemObject listItemObject = new ListItemObject(
                titles[i], genres[i], years[i], drawables[random.nextInt(4)]
            );
            listOfData.add(listItemObject);
        }

        return listOfData;
    }

    @Override
    public ListItemObject createNewListItem() {
        return new ListItemObject(
                titles[0], genres[0], years[0], drawables[random.nextInt(4)]
        );
    }

    @Override
    public void deleteListItem(ListItemObject listItemObject) {

    }

    @Override
    public void insertListItem(ListItemObject temporaryListItemObject) {
        
    }
}
