package com.ram0250.practice.oleg.sqlitedbapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ram0250.practice.oleg.sqlitedbapp.R;

public class FilmDetailActivity extends AppCompatActivity {

    private static final String EXTRA_TITLE = "EXTRA_TITLE";
    private static final String EXTRA_GENRES = "EXTRA_GENRES";
    private static final String EXTRA_YEAR = "EXTRA_YEAR";
    private static final String EXTRA_DRAWABLE = "EXTRA_DRAWABLE";

    private TextView title;
    private TextView genres;
    private TextView year;
    private View coloredBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);

        Intent i = getIntent();
        String titleExtra = i.getStringExtra(EXTRA_TITLE);
        String genresExtra = i.getStringExtra(EXTRA_GENRES);
        int yearExtra = i.getIntExtra(EXTRA_YEAR, 0);
        int drawableResourceExtra = i.getIntExtra(EXTRA_DRAWABLE, 0);

        title = findViewById(R.id.tv_film_title_header);
        title.setText(titleExtra);

        genres = findViewById(R.id.tv_film_content);
        genres.setText(genresExtra);

        coloredBackground = findViewById(R.id.imv_colored_background);
        coloredBackground.setBackgroundResource(drawableResourceExtra);
    }
}
