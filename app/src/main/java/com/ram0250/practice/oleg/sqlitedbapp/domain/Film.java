package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

@Entity(
        tableName = "films",
        indices = {
                @Index(value = "year"),
                @Index(value = "title")
        }
)
public class Film extends Identifiable{

    private String title;

    private int year;

    private int runtime;

    private String awards;

    public Film() { }

    @Ignore
    public Film(String title, int year, int runtime, String awards) {
        this.title = title;
        this.year = year;
        this.runtime = runtime;
        this.awards = awards;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public int getYear() { return year; }

    public void setYear(int year) { this.year = year; }

    public int getRuntime() { return runtime; }

    public void setRuntime(int runtime) { this.runtime = runtime; }

    public String getAwards() { return awards; }

    public void setAwards(String awards) { this.awards = awards; }
}
