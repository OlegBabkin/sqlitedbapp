package com.ram0250.practice.oleg.sqlitedbapp.pojo;

import java.util.List;

/**
 * This a contract between classes that dictate how they can talk to each other
 * without giving implementation Details.
 * Created by oleg.babkin on 17 Nov 2017.
 */

public interface DataSourceInterface {

    List<ListItemObject> getListOfData();

    ListItemObject createNewListItem();

    void deleteListItem(ListItemObject listItemObject);

    void insertListItem(ListItemObject temporaryListItemObject);
}
