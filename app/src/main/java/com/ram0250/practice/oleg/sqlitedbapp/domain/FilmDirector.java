package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "film_director",
        primaryKeys = {"film_id", "director_id"},
        foreignKeys = {
                @ForeignKey(
                        entity = Film.class,
                        parentColumns = "id",
                        childColumns = "film_id",
                        onDelete = CASCADE
                ),
                @ForeignKey(
                        entity = Director.class,
                        parentColumns = "id",
                        childColumns = "director_id",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index(value = "film_id"),
                @Index(value = "director_id")
        }
)
public class FilmDirector {
    @ColumnInfo(name = "film_id")
    private long filmId;

    @ColumnInfo(name = "director_id")
    private long directorId;

    public FilmDirector() { }

    @Ignore
    public FilmDirector(long filmId, long directorId) {
        this.filmId = filmId;
        this.directorId = directorId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getDirectorId() {
        return directorId;
    }

    public void setDirectorId(long directorId) {
        this.directorId = directorId;
    }
}
