package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Language;

import java.util.List;

@Dao
public interface LanguageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Language... languages);

    @Update
    void updateMany(Language... languages);

    @Delete
    void deleteMany(Language... languages);

    @Query("SELECT * FROM languages")
    List<Language> getAll();
}
