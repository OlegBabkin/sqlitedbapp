package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

@Entity(tableName = "writers")
public class Writer extends Person{

    public Writer() { }

    @Ignore
    public Writer(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
