package com.ram0250.practice.oleg.sqlitedbapp.view;

import android.view.View;

import com.ram0250.practice.oleg.sqlitedbapp.pojo.ListItemObject;

import java.util.List;

/**
 * Created by oleg.babkin on 17 Nov 2017.
 */

public interface ViewInterface {

    void startDetailActivity(String title, String genres, int year, int colorR, View viewRoot);

    void setUpAdapterAndView(List<ListItemObject> listOfData);

    void addNewListItemToView(ListItemObject newListItem);

    void deleteListItemAt(int position);

    void showUndoSnackbar();

    void insertListItemAt(int position, ListItemObject listItemObject);
}
