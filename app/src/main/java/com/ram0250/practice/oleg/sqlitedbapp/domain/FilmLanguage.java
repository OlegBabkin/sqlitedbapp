package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "film_language",
        primaryKeys = {"film_id", "language_id"},
        foreignKeys = {
                @ForeignKey(
                        entity = Film.class,
                        parentColumns = "id",
                        childColumns = "film_id",
                        onDelete = CASCADE
                ),
                @ForeignKey(
                        entity = Language.class,
                        parentColumns = "id",
                        childColumns = "language_id",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index(value = "film_id"),
                @Index(value = "language_id")
        }
)
public class FilmLanguage {
    @ColumnInfo(name = "film_id")
    private long filmId;

    @ColumnInfo(name = "language_id")
    private long languageId;

    public FilmLanguage() { }

    @Ignore
    public FilmLanguage(long filmId, long languageId) {
        this.filmId = filmId;
        this.languageId = languageId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }
}
