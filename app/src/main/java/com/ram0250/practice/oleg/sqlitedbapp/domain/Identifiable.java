package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.PrimaryKey;

abstract class Identifiable {
    @PrimaryKey(autoGenerate = true)
    protected long id;

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }
}
