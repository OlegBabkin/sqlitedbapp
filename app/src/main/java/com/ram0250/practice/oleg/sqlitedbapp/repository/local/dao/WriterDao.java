package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Writer;

import java.util.List;

@Dao
public interface WriterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Writer... writers);

    @Update
    void updateMany(Writer... writers);

    @Delete
    void deleteMany(Writer... writers);

    @Query("SELECT * FROM writers")
    List<Writer> getAll();
}
