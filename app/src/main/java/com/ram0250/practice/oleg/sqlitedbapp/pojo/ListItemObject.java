package com.ram0250.practice.oleg.sqlitedbapp.pojo;

public class ListItemObject {

    private String title;
    private String genres;
    private int year;
    private int colorR;

    public ListItemObject() { }

    public ListItemObject(String title, String genres, int year, int colorR) {
        this.title = title;
        this.genres = genres;
        this.year = year;
        this.colorR = colorR;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getColorR() {
        return colorR;
    }

    public void setColorR(int colorR) {
        this.colorR = colorR;
    }
}
