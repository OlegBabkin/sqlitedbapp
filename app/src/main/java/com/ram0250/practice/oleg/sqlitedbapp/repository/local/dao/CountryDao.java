package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Country;

import java.util.List;

@Dao
public interface CountryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Country... countries);

    @Update
    void updateMany(Country... countries);

    @Delete
    void deleteMany(Country... countries);

    @Query("SELECT * FROM countries")
    List<Country> getAll();
}
