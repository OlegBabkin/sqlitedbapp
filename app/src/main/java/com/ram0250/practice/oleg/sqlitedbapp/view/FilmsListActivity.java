package com.ram0250.practice.oleg.sqlitedbapp.view;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.transition.Fade;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ram0250.practice.oleg.sqlitedbapp.R;
import com.ram0250.practice.oleg.sqlitedbapp.logic.Controller;
import com.ram0250.practice.oleg.sqlitedbapp.pojo.FakeDataSource;
import com.ram0250.practice.oleg.sqlitedbapp.pojo.ListItemObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FilmsListActivity extends AppCompatActivity implements ViewInterface, View.OnClickListener {

    private static final String EXTRA_TITLE = "EXTRA_TITLE";
    private static final String EXTRA_GENRES = "EXTRA_GENRES";
    private static final String EXTRA_YEAR = "EXTRA_YEAR";
    private static final String EXTRA_DRAWABLE = "EXTRA_DRAWABLE";

    private List<ListItemObject> listOfData;

    private LayoutInflater layoutInflater;
    private RecyclerView recyclerView;
    private CustomAdapter adapter;
    private Toolbar toolbar;

    private Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_films_list);

        recyclerView = findViewById(R.id.rec_list_films);
        layoutInflater = getLayoutInflater();
        toolbar = findViewById(R.id.toolbar_films);
        toolbar.setTitle(R.string.title_toolbar);
        toolbar.setLogo(R.drawable.ic_view_list_white_24dp);
        toolbar.setTitleMarginStart(72);

        FloatingActionButton fabCreate = findViewById(R.id.fab_create_new_item);
        fabCreate.setOnClickListener(this);

        // DI
        controller = new Controller(this, new FakeDataSource());
    }

    @Override
    public void startDetailActivity(String title, String genres, int year, int drawable, View viewRoot) {
        Intent i = new Intent(this, FilmDetailActivity.class);
        i.putExtra(EXTRA_TITLE, title);
        i.putExtra(EXTRA_GENRES, genres);
        i.putExtra(EXTRA_YEAR, year);
        i.putExtra(EXTRA_DRAWABLE, drawable);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(new Fade(Fade.IN));
            getWindow().setEnterTransition(new Fade(Fade.OUT));

            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
                    new Pair<View, String>(viewRoot.findViewById(R.id.imv_list_item_circle),
                            getString(R.string.transition_drawable)),
                    new Pair<View, String>(viewRoot.findViewById(R.id.tv_item_genre),
                            getString(R.string.transition_genres)),
                    new Pair<View, String>(viewRoot.findViewById(R.id.tv_item_title),
                            getString(R.string.transition_title)),
                    new Pair<View, String>(viewRoot.findViewById(R.id.tv_item_year),
                            getString(R.string.transition_year)));
            startActivity(i, options.toBundle());
        } else {
            startActivity(i);
        }
    }

    @Override
    public void setUpAdapterAndView(List<ListItemObject> listOfData) {
        this.listOfData = listOfData;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new CustomAdapter();
        recyclerView.setAdapter(adapter);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                linearLayoutManager.getOrientation()
        );

        itemDecoration.setDrawable(ContextCompat.getDrawable(
                FilmsListActivity.this,
                R.drawable.divider_white
        ));

        recyclerView.addItemDecoration(itemDecoration);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallback());
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void addNewListItemToView(ListItemObject newListItem) {
        listOfData.add(newListItem);
        int endOfList = listOfData.size() - 1;
        adapter.notifyItemInserted(endOfList);
        recyclerView.smoothScrollToPosition(endOfList);
    }

    @Override
    public void deleteListItemAt(int position) {
        listOfData.remove(position);
        adapter.notifyItemRemoved(position);
    }

    @Override
    public void showUndoSnackbar() {
        Snackbar.make(
                findViewById(R.id.root_films_list),
                getString(R.string.action_delete_item),
                Snackbar.LENGTH_LONG
        ).setAction(R.string.action_undo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.onUndoConfirmed();
            }
        }).addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                controller.onSnackbarTimeout();
            }
        }).show();
    }

    @Override
    public void insertListItemAt(int position, ListItemObject listItemObject) {
        listOfData.add(position, listItemObject);
        adapter.notifyItemInserted(position);
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if (viewId == R.id.fab_create_new_item) {
            controller.createNewListItem();
        }
    }

    private class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> {

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = layoutInflater.inflate(R.layout.item_detail, parent, false);
            return new CustomViewHolder(v);
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            ListItemObject currentListItemObject = listOfData.get(position);
            holder.coloredCircle.setImageResource(currentListItemObject.getColorR());
            holder.title.setText(currentListItemObject.getTitle());
            holder.genres.setText(currentListItemObject.getGenres());
            holder.year.setText(String.valueOf(currentListItemObject.getYear()));
            holder.loading.setVisibility(View.INVISIBLE);
        }

        @Override
        public int getItemCount() {
            return listOfData.size();
        }

        class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private CircleImageView coloredCircle;
            private TextView title;
            private TextView genres;
            private TextView year;
            private ViewGroup container;
            private ProgressBar loading;

            public CustomViewHolder(View itemView) {
                super(itemView);

                this.coloredCircle = itemView.findViewById(R.id.imv_list_item_circle);
                this.title = itemView.findViewById(R.id.tv_item_title);
                this.genres = itemView.findViewById(R.id.tv_item_genre);
                this.year = itemView.findViewById(R.id.tv_item_year);
                this.loading = itemView.findViewById(R.id.loading_item_data);
                this.container = itemView.findViewById(R.id.root_list_item);
                this.container.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                ListItemObject listItemObject = listOfData.get(this.getAdapterPosition());
                controller.onListItemClick(listItemObject, view);
            }
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        /*First Param is for Up/Down motion, second is for Left/Right.
        Note that we can supply 0, one constant (e.g. ItemTouchHelper.LEFT), or two constants (e.g.
        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) to specify what directions are allowed.
        */
        return new ItemTouchHelper.SimpleCallback(
                0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            //not used, as the first parameter above is 0
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }


            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                controller.onListItemSwiped(position, listOfData.get(position));
            }
        };
    }
}
