package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "film_country",
        primaryKeys = {"film_id", "country_id"},
        foreignKeys = {
                @ForeignKey(
                        entity = Film.class,
                        parentColumns = "id",
                        childColumns = "film_id",
                        onDelete = CASCADE
                ),
                @ForeignKey(
                        entity = Country.class,
                        parentColumns = "id",
                        childColumns = "country_id",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index(value = "film_id"),
                @Index(value = "country_id")
        }
)
public class FilmCountry {

    @ColumnInfo(name = "film_id")
    private long filmId;

    @ColumnInfo(name = "country_id")
    private long countryId;

    public FilmCountry() { }

    @Ignore
    public FilmCountry(long filmId, long countryId) {
        this.filmId = filmId;
        this.countryId = countryId;
    }

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }
}
