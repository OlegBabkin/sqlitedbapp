package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;

abstract class Person extends Identifiable {

    @ColumnInfo(name = "first_name")
    protected String firstname;

    @ColumnInfo(name = "last_name")
    protected String lastname;

    public String getFirstname() { return firstname; }

    public void setFirstname(String firstname) { this.firstname = firstname; }

    public String getLastname() { return lastname; }

    public void setLastname(String lastname) { this.lastname = lastname; }
}
