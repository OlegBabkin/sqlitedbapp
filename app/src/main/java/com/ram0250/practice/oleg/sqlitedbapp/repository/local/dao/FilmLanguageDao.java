package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmLanguage;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Language;

import java.util.List;

@Dao
public interface FilmLanguageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(FilmLanguage... filmLanguages);

    @Update
    void updateMany(FilmLanguage... filmLanguages);

    @Delete
    void deleteMany(FilmLanguage... filmLanguages);

    @Query(
            "SELECT languages.* FROM languages\n"
                    + "INNER JOIN film_language ON languages.id = film_language.language_id\n"
                    + "WHERE film_language.film_id = :id"
    )
    List<Language> getLanguagesByFilmId(long id);

    @Query(
            "SELECT films.* FROM films\n"
                    + "INNER JOIN film_language ON films.id = film_language.film_id\n"
                    + "WHERE film_language.language_id = :id"
    )
    List<Film> getFilmsByLanguageId(long id);
}
