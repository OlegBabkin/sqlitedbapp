package com.ram0250.practice.oleg.sqlitedbapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Actor;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Country;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmCountry;
import com.ram0250.practice.oleg.sqlitedbapp.repository.local.database.FilmsDatabase;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
