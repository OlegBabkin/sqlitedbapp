package com.ram0250.practice.oleg.sqlitedbapp.repository.local.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.ram0250.practice.oleg.sqlitedbapp.domain.*;
import com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao.*;

@Database(
        entities = {
                Actor.class,
                Country.class,
                Director.class,
                Film.class,
                FilmActor.class,
                FilmCountry.class,
                FilmDirector.class,
                FilmGenre.class,
                FilmLanguage.class,
                FilmWriter.class,
                Genre.class,
                Language.class,
                Writer.class
        },
        version = 1,
        exportSchema = false
)
public abstract class FilmsDatabase extends RoomDatabase {

    private static final String DB_NAME = "films_db.db";
    private static FilmsDatabase INSTANCE;

    public abstract ActorDao getActorDao();
    public abstract CountryDao getCountryDao();
    public abstract DirectorDao getDirectorDao();
    public abstract FilmDao getFilmDao();
    public abstract FilmActorDao getFilmActorDao();
    public abstract FilmCountryDao getFilmCountryDao();
    public abstract FilmDirectorDao getFilmDirectorDao();
    public abstract FilmGenreDao getFilmGenreDao();
    public abstract FilmLanguageDao getFilmLanguageDao();
    public abstract FilmWriterDao getFilmWriterDao();
    public abstract GenreDao getGenreDao();
    public abstract LanguageDao getLanguageDao();
    public abstract WriterDao getWriterDao();

    public static FilmsDatabase getFilmsDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room
                    .databaseBuilder(context.getApplicationContext(), FilmsDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
