package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Director;

import java.util.List;

@Dao
public interface DirectorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Director... directors);

    @Update
    void updateMany(Director... directors);

    @Delete
    void deleteMany(Director... directors);

    @Query("SELECT * FROM directors")
    List<Director> getAll();
}
