package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

@Entity(tableName = "directors")
public class Director extends Person {

    public Director() { }

    @Ignore
    public Director(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
