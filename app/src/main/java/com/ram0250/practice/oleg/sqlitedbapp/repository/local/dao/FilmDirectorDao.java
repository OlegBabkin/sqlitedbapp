package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Director;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmDirector;

import java.util.List;

@Dao
public interface FilmDirectorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(FilmDirector... filmDirectors);

    @Update
    void updateMany(FilmDirector... filmDirectors);

    @Delete
    void deleteMany(FilmDirector... filmDirectors);

    @Query(
            "SELECT directors.* FROM directors\n"
                    + "INNER JOIN film_director ON directors.id = film_director.director_id\n"
                    + "WHERE film_director.film_id = :id"
    )
    List<Director> getDirectorsByFilmId(long id);

    @Query(
            "SELECT films.* FROM films\n"
                    + "INNER JOIN film_director ON films.id = film_director.film_id\n"
                    + "WHERE film_director.director_id = :id"
    )
    List<Film> getFilmsByDirectorId(long id);
}
