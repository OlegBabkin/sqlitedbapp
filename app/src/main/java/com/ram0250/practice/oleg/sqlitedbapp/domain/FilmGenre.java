package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "film_genre",
        primaryKeys = {"film_id", "genre_id"},
        foreignKeys = {
                @ForeignKey(
                        entity = Film.class,
                        parentColumns = "id",
                        childColumns = "film_id",
                        onDelete = CASCADE
                ),
                @ForeignKey(
                        entity = Genre.class,
                        parentColumns = "id",
                        childColumns = "genre_id",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index(value = "film_id"),
                @Index(value = "genre_id")
        }
)
public class FilmGenre {
    @ColumnInfo(name = "film_id")
    private long filmId;

    @ColumnInfo(name = "genre_id")
    private long genreId;

    public FilmGenre() { }

    @Ignore
    public FilmGenre(long filmId, long genreId) {
        this.filmId = filmId;
        this.genreId = genreId;
    }

    public long getFilmId() { return filmId; }

    public void setFilmId(long filmId) { this.filmId = filmId; }

    public long getGenreId() { return genreId; }

    public void setGenreId(long genreId) { this.genreId = genreId; }
}
