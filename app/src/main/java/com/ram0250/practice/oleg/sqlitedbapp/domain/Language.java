package com.ram0250.practice.oleg.sqlitedbapp.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

@Entity(tableName = "languages")
public class Language extends Identifiable {

    @ColumnInfo(name = "language_name")
    private String languageName;

    public Language() { }

    @Ignore
    public Language(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageName() { return languageName; }

    public void setLanguageName(String languageName) { this.languageName = languageName; }
}
