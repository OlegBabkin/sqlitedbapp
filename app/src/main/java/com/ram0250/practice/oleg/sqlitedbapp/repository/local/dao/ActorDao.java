package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Actor;

import java.util.List;

@Dao
public interface ActorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(Actor... actors);

    @Update
    void updateMany(Actor... actors);

    @Delete
    void deleteMany(Actor... actors);

    @Query("SELECT * FROM actors")
    List<Actor> getAll();
}
