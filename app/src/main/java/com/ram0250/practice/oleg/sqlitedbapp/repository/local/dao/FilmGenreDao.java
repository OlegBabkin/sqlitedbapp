package com.ram0250.practice.oleg.sqlitedbapp.repository.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ram0250.practice.oleg.sqlitedbapp.domain.Film;
import com.ram0250.practice.oleg.sqlitedbapp.domain.FilmGenre;
import com.ram0250.practice.oleg.sqlitedbapp.domain.Genre;

import java.util.List;

@Dao
public interface FilmGenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMany(FilmGenre... filmGenres);

    @Update
    void updateMany(FilmGenre... filmGenres);

    @Delete
    void deleteMany(FilmGenre... filmGenres);

    @Query(
            "SELECT genres.* FROM genres\n"
                    + "INNER JOIN film_genre ON genres.id = film_genre.genre_id\n"
                    + "WHERE film_genre.film_id = :id"
    )
    List<Genre> getGenresByFilmId(long id);

    @Query(
            "SELECT films.* FROM films\n"
                    + "INNER JOIN film_genre ON films.id = film_genre.film_id\n"
                    + "WHERE film_genre.genre_id = :id"
    )
    List<Film> getFilmsByGenreId(long id);
}
